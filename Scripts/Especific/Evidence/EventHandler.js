﻿$(function () {
    obtenerlistProductos('#sltP');
    $('#sltP').change(function (e) {
        var id = $(this).val();
        $('#sltC').empty();
        $('#sltC').append('<option value="0">Seleccione una ciudad...</option>');
        obtenerlistCiudades(id);
    });
});

$("#btnContinuarTZ").click('click', function () {
    var idC, idTZ;

    if ($('#blnCarga').prop('checked') && $('#blnDocumentos').prop('checked')) {        
        idC = $('#sltC').val();
        idTZ = 10;
        $('#sltZ').empty();
        obtenerlistZone(idTZ, idC);        
    } else if ($('#blnCarga').prop('checked')) {
        idC = $('#sltC').val();
        idTZ = 2;
        $('#sltZ').empty();
        obtenerlistZone(idTZ, idC);        
    } else if ($('#blnDocumentos').prop('checked')) {
        idC = $('#sltC').val();
        idTZ = 2;
        $('#sltZ').empty();
        obtenerlistZone(idTZ, idC);        
    }
});

$("#save").click('click', function () {
    var Datos = new Object();
    debugger;
    Datos['intIdCiudad'] = parseInt($('#sltC').val());
    Datos['shtIdProducto']= $('#sltP').val();
    Datos['blnDistribucion'] = $('#blnDistribucion').prop('checked');
    Datos['blnRecoleccion'] = $('#blnRecoleccion').prop('checked');
    Datos['blnFirma'] = $('#blnFirma').prop('checked');
    Datos['blnImagen'] = $('#blnImagen').prop('checked');
    Datos['blnGuia'] = $('#blnGuia').prop('checked');
    Datos['blnCarga'] = $('#blnCarga').prop('checked');
    Datos['blnDocumentos'] = $('#blnDocumentos').prop('checked');
    if ($('#blnDistribucion').prop('checked') && $('#blnRecoleccion').prop('checked')) {
        Datos['blnReportadoRecogido'] = $('#blnReportadoRecogido').prop('checked');
        Datos['blnReportadoEntregado'] = $('#blnReportadoEntregado').prop('checked');
        Datos['blnReportadoDevuelto'] = $('#blnReportadoDevuelto').prop('checked');
        Datos['blnIdNoRecogido'] = $('#blnIdNoRecogido').prop('checked');
    } else if ($('#blnDistribucion').prop('checked')) {
        Datos['blnReportadoEntregado'] = $('#blnReportadoEntregadoD').prop('checked');
        Datos['blnReportadoDevuelto'] = $('#blnReportadoDevueltoD').prop('checked');
    } else if ($('#blnRecoleccion').prop('checked')) {
        Datos['blnReportadoRecogido'] = $('#blnReportadoRecogidoR').prop('checked');
        Datos['blnIdNoRecogido'] = $('#blnIdNoRecogidoR').prop('checked');
    }

    var selectValue = $('#sltZ').val().join(';') || [];
    Datos['strZonas'] = selectValue;
    if ($('#blnFirma').prop('checked') || $('#blnImagen').prop('checked') || $('#blnGuia').prop('checked')) {
        saveParameters(Datos);
    }
    

});