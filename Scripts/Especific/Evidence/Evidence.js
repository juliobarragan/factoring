﻿function obtenerlistProductos(ddl) {
    try {
        $.ajax(
            {
                url: "/Evidence/listProduct",
                method: "POST",
                dataType: 'json',
                async: false,
                content: "application/json; charset=utf-8",
                success: function (result) {
                    if (result.Success) {
                        $(ddl).append(
                            '<option selected value="0">Seleccione un producto...</option>'
                        );
                        for (var i = 0; i < result.data.length; i++) {
                            $(ddl).append(
                                '<option value="' + result.data[i].id + '">' + result.data[i].nombre + '</option>'
                            );
                        }
                    }
                    else { alertify.error("No hay productos disponibles.") }
                }
            });
    } catch (e) {
        alertify.error("No hay ciudades disponibles..")
    }
}

function obtenerlistCiudades(idSeleccionado) {
    try {
        $.ajax(
            {
                url: "/Evidence/listCities",
                method: "POST",
                dataType: 'json',
                data: { id: idSeleccionado },
                async: false,
                content: "application/json; charset=utf-8",
                success: function (result) {
                    if (result.Success) {                        
                        for (var i = 0; i < result.data.length; i++) {                            
                            $('#sltC').append('<option value="' + result.data[i].id + '">' + result.data[i].nombre + '</option>');
                        }                        
                    }
                    else { alertify.error("No hay ciudades disponibles.") }
                }
            });
    } catch (e) {
        alertify.error("No hay ciudades disponibles..")
    }    
}

function obtenerlistZone(idTZ, idC) {
    try {
        $.ajax(
            {
                url: "/Evidence/listZone",
                method: "POST",
                dataType: 'json',
                data: { idTypeZone: idTZ, idCity: idC },
                async: false,
                content: "application/json; charset=utf-8",
                success: function (result) {
                    if (result.Success) {
                        for (var i = 0; i < result.data.length; i++) {
                            $('#sltZ').append('<option value="' + result.data[i].id + '">' + result.data[i].nombre + '</option>');
                        }
                    }
                    if (result.data.length == 0) {
                        alertify.error("No hay Zonas para este proceso.")
                    }

                }
            });
    } catch (e) {
        alertify.error("No hay zonas disponibles..")
    }
}

function saveParameters(Datos) {
    debugger;
    try {
        var model = Datos;
        $.ajax(
            {
                url: "/Evidence/SaveParameters",
                method: "POST",
                dataType: 'json',
                data: model ,
                async: false,
                content: "application/json; charset=utf-8",
                success: function (result) {
                    debugger;
                    if (result.Success) {
                        alertify.success("Exito al guardar.");
                    } else {
                        alertify.error("Fallo al guardar.");
                    }
                }
            });
    } catch (e) {
        alertify.error("Fallo al guardar el proceso.")
    }
}