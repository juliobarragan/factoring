﻿$(document).ready(function () {

    $('#search').click("click", function () {
        if ($('#sltC').val() == 0) {
            alertify.error("Seleccione una ciudad.");
            $('#search').attr("disabled", true);
            setTimeout(function () { $('#search').attr("disabled", false); }, 1500);
        }
        if ($('#sltP').val() == 0) {
            alertify.error("Seleccione un producto.");
            $('#search').attr("disabled", true);
            setTimeout(function () { $('#search').attr("disabled", false); }, 1500);
        }
        if ($('#sltP').val() != 0 && $('#sltC').val() != 0) {
            //alertify.success("Producto y cuidad seleccionados.");
            $('#search').attr("disabled", true);
            $('#sltP').attr("disabled", true);
            $('#sltC').attr("disabled", true);
            $('#divTZ').removeClass('oculto');
        }
    });

    $('#btnContinuarTZ').click("click", function (event) {
        event.preventDefault();
        if ($('#blnCarga').prop('checked') || $('#blnDocumentos').prop('checked')) {
            //alertify.success("Tipo de zona seleccionada.");
            $('#btnContinuarTZ').attr("disabled", true);
            $('#blnCarga').attr("disabled", true);
            $('#blnDocumentos').attr("disabled", true);
            $('#divNZ').removeClass('oculto');
            $('#btnCambiarTZ').show();
        } else {
            alertify.error("Seleccione uno o varios tipos de zonas.");
            $('#btnContinuarTZ').attr("disabled", true);
            setTimeout(function () { $('#btnContinuarTZ').attr("disabled", false); }, 1500);
        }
    });

    $('#btnCambiarTZ').click("click", function (event) {
        event.preventDefault();
        $('#btnContinuarTZ').attr("disabled", false);
        $('#blnCarga').attr("disabled", false);
        $('#blnDocumentos').attr("disabled", false);
    });

    $('#btnContinuarNZ').click("click", function () {        
        if ($('#sltZ').val() != 0) {
            //alertify.success("Una o más zonas seleccionadas.");
            $('#btnContinuarNZ').attr("disabled", true);
            $('#sltZ').attr("disabled", true);
            $('#btnSTsltZ').attr("disabled", true);
            $('#divTL').removeClass('oculto');
            $('#btnCambiarNZ').show();
        } else {
            alertify.error("Selecciona una o varias zonas.");
            $('#btnContinuarNZ').attr("disabled", true);
            setTimeout(function () { $('#btnContinuarNZ').attr("disabled", false); }, 1500);
        }
    });

    $('#btnCambiarNZ').click("click", function (event) {
        event.preventDefault();
        $('#btnContinuarNZ').attr("disabled", false);
        $('#sltZ').attr("disabled", false);
        $('#btnSTsltZ').attr("disabled", false);
    });

    $('#btnContinuarTL').click("click", function () {
        if ($('#blnDistribucion').prop('checked') && $('#blnRecoleccion').prop('checked')) {
            //alertify.success("Todas los tipos de logistica estan seleccionadas.");
            $('#btnContinuarTL').attr("disabled", true);
            $('#blnDistribucion').attr("disabled", true);
            $('#blnRecoleccion').attr("disabled", true);
            $('#divTM').removeClass('oculto');
            $('#divTMD').addClass('oculto');
            $('#divTMR').addClass('oculto');
            $('#btnCambiarTL').show();
        } else if ($('#blnDistribucion').prop('checked')) {
            //alertify.success("Distribucion seleccionado.");
            $('#btnContinuarTL').attr("disabled", true);
            $('#blnDistribucion').attr("disabled", true);
            $('#blnRecoleccion').attr("disabled", true);
            $('#divTM').addClass('oculto');
            $('#divTMD').removeClass('oculto');
            $('#divTMR').addClass('oculto');
            $('#btnCambiarTL').show();
        } else if ($('#blnRecoleccion').prop('checked')) {
            //alertify.success("Todas los tipos de logistica estan seleccionadas.");
            $('#btnContinuarTL').attr("disabled", true);
            $('#blnDistribucion').attr("disabled", true);
            $('#blnRecoleccion').attr("disabled", true);
            $('#divTM').addClass('oculto');
            $('#divTMR').removeClass('oculto');
            $('#divTMD').addClass('oculto');
            $('#btnCambiarTL').show();
        } else {
            alertify.error("Seleccione un tipo de logistica.");
            $('#btnContinuarTL').attr("disabled", true);
            setTimeout(function () { $('#btnContinuarTL').attr("disabled", false); }, 1500);
        }

    });

    $('#btnCambiarTL').click("click", function (event) {
        event.preventDefault();
        $('#btnContinuarTL').attr("disabled", false);
        $('#blnDistribucion').attr("disabled", false);
        $('#blnRecoleccion').attr("disabled", false);
    });

    $('#btnContinuarTM').click("click", function () {
        if ($('#blnReportadoEntregado').prop('checked') || $('#blnReportadoDevuelto').prop('checked')) {
            //alertify.success("Tipo de movimiento distribución seleccionado.");
            $('#btnContinuarTM').attr("disabled", true);
        } else {
            alertify.error("Seleccione un tipo de movimiento de distribución.");
            $('#btnContinuarTM').attr("disabled", true);
            setTimeout(function () { $('#btnContinuarTM').attr("disabled", false); }, 1500);
        }

        if ($('#blnReportadoRecogido').prop('checked') || $('#blnIdNoRecogido').prop('checked')) {
            //alertify.success("Tipo de movimiento recoleccion seleccionado.");
            $('#btnContinuarTM').attr("disabled", true);
        } else {
            alertify.error("Seleccione un tipo de movimiento de recoleccion.");
            $('#btnContinuarTM').attr("disabled", true);
            setTimeout(function () { $('#btnContinuarTM').attr("disabled", false); }, 1500);
        }

        if ($('#blnReportadoEntregado').prop('checked') || $('#blnReportadoDevuelto').prop('checked')){
            if ($('#blnReportadoRecogido').prop('checked') || $('#blnIdNoRecogido').prop('checked')) {
                    $('#btnContinuarTM').attr("disabled", true);
                    $('#blnReportadoRecogido').attr("disabled", true);
                    $('#blnIdNoRecogido').attr("disabled", true);
                    $('#blnReportadoEntregado').attr("disabled", true);
                    $('#blnReportadoDevuelto').attr("disabled", true);
                    $('#divE').removeClass('oculto');
                    $('#btnCambiarTM').show();
            }            
        } else {
            $('#btnContinuarTM').attr("disabled", true);
            setTimeout(function () { $('#btnContinuarTM').attr("disabled", false); }, 1500);
        }
    });

    $('#btnCambiarTM').click("click", function (event) {
        event.preventDefault();
        $('#btnContinuarTM').attr("disabled", false);
        $('#blnReportadoRecogido').attr("disabled", false);
        $('#blnIdNoRecogido').attr("disabled", false);
        $('#blnReportadoEntregado').attr("disabled", false);
        $('#blnReportadoDevuelto').attr("disabled", false);
    });

    $('#btnContinuarTMD').click("click", function () {
        if ($('#blnReportadoEntregadoD').prop('checked') || $('#blnReportadoDevueltoD').prop('checked')) {
            //alertify.success("Tipo de movimiento distribución seleccionado.");
            $('#btnContinuarTMD').attr("disabled", true);
            $('#blnReportadoEntregadoD').attr("disabled", true);
            $('#blnReportadoDevueltoD').attr("disabled", true);
            $('#divE').removeClass('oculto');
            $('#btnCambiarTMD').show();
        } else {
            alertify.error("Seleccione un tipo de movimiento de distribución.");
            $('#btnContinuarTMD').attr("disabled", true);
            setTimeout(function () { $('#btnContinuarTMD').attr("disabled", false); }, 1500);
        }
    });

    $('#btnCambiarTMD').click("click", function (event) {
        event.preventDefault();
        $('#btnContinuarTMD').attr("disabled", false);
        $('#blnReportadoEntregadoD').attr("disabled", false);
        $('#blnReportadoDevueltoD').attr("disabled", false);
    });

    $('#btnContinuarTMR').click("click", function () {
        if ($('#blnReportadoRecogidoR').prop('checked') || $('#blnIdNoRecogidoR').prop('checked')) {
            //alertify.success("Tipo de movimiento recolección seleccionado.");
            $('#btnContinuarTMR').attr("disabled", true);
            $('#blnReportadoRecogidoR').attr("disabled", true);
            $('#blnIdNoRecogidoR').attr("disabled", true);
            $('#divE').removeClass('oculto');
            $('#btnCambiarTMR').show();
        } else {
            alertify.error("Seleccione un tipo de movimiento de recolección.");
            $('#btnContinuarTMR').attr("disabled", true);
            setTimeout(function () { $('#btnContinuarTMR').attr("disabled", false); }, 1500);
        }
    });

    $('#btnCambiarTMR').click("click", function (event) {
        event.preventDefault();
        $('#btnContinuarTMR').attr("disabled", false);
        $('#blnReportadoRecogidoR').attr("disabled", false);
        $('#blnIdNoRecogidoR').attr("disabled", false);
    });

    $('#save').click("click", function () {
        if ($('#blnFirma').prop('checked') || $('#blnImagen').prop('checked') || $('#blnGuia').prop('checked')) {
            //alertify.success("Una o más evidencias seleccionadas.");
            $('#search').attr("disabled", true);
            $('#sltP').attr("disabled", true);
            $('#sltC').attr("disabled", true);
            $('#btnContinuarTZ').attr("disabled", true);
            $('#blnCarga').attr("disabled", true);
            $('#blnDocumentos').attr("disabled", true);
            $('#btnContinuarNZ').attr("disabled", true);
            $('#sltZ').attr("disabled", true);
            $('#btnSTsltZ').attr("disabled", true);
            $('#btnContinuarTL').attr("disabled", true);
            $('#blnDistribucion').attr("disabled", true);
            $('#blnRecoleccion').attr("disabled", true);
            $('#btnContinuarTM').attr("disabled", true);
            $('#blnReportadoRecogido').attr("disabled", true);
            $('#blnIdNoRecogido').attr("disabled", true);
            $('#blnReportadoEntregado').attr("disabled", true);
            $('#blnReportadoDevuelto').attr("disabled", true);
            $('#btnContinuarTMD').attr("disabled", true);
            $('#blnReportadoEntregadoD').attr("disabled", true);
            $('#blnReportadoDevueltoD').attr("disabled", true);
            $('#btnContinuarTMR').attr("disabled", true);
            $('#blnReportadoRecogidoR').attr("disabled", true);
            $('#blnIdNoRecogidoR').attr("disabled", true);
            $('#save').attr("disabled", true);
            $('#blnFirma').attr("disabled", true);
            $('#blnImagen').attr("disabled", true);
            $('#blnGuia').attr("disabled", true);
            $('#btnCambiarTZ').attr("disabled", true);
            $('#btnCambiarNZ').attr("disabled", true);
            $('#btnCambiarTL').attr("disabled", true);
            $('#btnCambiarTMD').attr("disabled", true);
            $('#btnCambiarTMR').attr("disabled", true);
            $('#btnCambiarTM').attr("disabled", true);
        } else {
            alertify.error("Seleccione una o más evidencias.");
            $('#save').attr("disabled", true);
            setTimeout(function () { $('#save').attr("disabled", false); }, 1500);
        }
    });

});

function seleccionarTodo(id) {
    $("#" + id + " option").each(function () {
        $("#" + id + " option[value=" + this.value + "]").prop("selected", true);
    });
}

