﻿$(document).ready(function () {
    $('#search').click("click", function () {
        if ($('#sltN').val() == 0) {
            alertify.error("Seleccione un NIT.");
            $('#search').attr("disabled", true);
            setTimeout(function () { $('#search').attr("disabled", false); }, 3000);
        }
        if ($('#sltC').val() == 0) {
            alertify.error("Seleccione un cliente.");
            $('#search').attr("disabled", true);
            setTimeout(function () { $('#search').attr("disabled", false); }, 3000);
        }
        if ($('#sltCS').val() == 0) {
            alertify.error("Seleccione un código SER.");
            $('#search').attr("disabled", true);
            setTimeout(function () { $('#search').attr("disabled", false); }, 3000);
        }
        if ($('#sltN').val() != 0 && $('#sltC').val() != 0 && $('#sltCS').val() != 0) {
            alertify.success("Filtros pricipales seleccionados.");
            $('#search').attr("disabled", true);
            $('#sltN').attr("disabled", true);
            $('#sltC').attr("disabled", true);
            $('#sltCS').attr("disabled", true);
            $('#divP').show();
        }
    });

    $('#btnContinuarP').click("click", function () {
        if ($('#chkDU').prop('checked') || $('#chkMI').prop('checked') || $('#chkMV').prop('checked') || $('#chkMP').prop('checked') || $('#chkTS').prop('checked') || $('#chkDL').prop('checked')) {
            alertify.success("Productos seleccionados.");
            $('#btnContinuarP').attr("disabled", true);
            $('#chkDU').attr("disabled", true);
            $('#chkMI').attr("disabled", true);
            $('#chkMV').attr("disabled", true);
            $('#chkMP').attr("disabled", true);
            $('#chkTS').attr("disabled", true);
            $('#chkDL').attr("disabled", true);
            $('#divC').show();
        } else {
            alertify.error("Seleccione uno o más productos.");
            $('#btnContinuarP').attr("disabled", true);
            setTimeout(function () { $('#btnContinuarP').attr("disabled", false); }, 3000);
        }            
    });

    $('#btnContinuarC').click("click", function () {
        if ($('#chkB').prop('checked') || $('#chkM').prop('checked') || $('#chkP').prop('checked') || $('#chkBA').prop('checked') || $('#chkC').prop('checked') || $('#chkBU').prop('checked')) {
            alertify.success("Ciudades seleccionadas.");
            $('#btnContinuarC').attr("disabled", true);
            $('#chkB').attr("disabled", true);
            $('#chkM').attr("disabled", true);
            $('#chkP').attr("disabled", true);
            $('#chkBA').attr("disabled", true);
            $('#chkC').attr("disabled", true);
            $('#chkBU').attr("disabled", true);
            $('#divTZ').show();
        } else {
            alertify.error("Seleccione una o más ciudades.");
            $('#btnContinuarC').attr("disabled", true);
            setTimeout(function () { $('#btnContinuarC').attr("disabled", false); }, 3000);
        }
    });

    $('#btnContinuarTZ').click("click", function () {
        if ($('#chkCarga').prop('checked') || $('#chkDoc').prop('checked')) {
            alertify.success("Zona seleccionada.");
            $('#btnContinuarTZ').attr("disabled", true);
            $('#chkCarga').attr("disabled", true);
            $('#chkDoc').attr("disabled", true);
            $('#divNZ').show();
        } else {
            alertify.error("Seleccione uno o más tipos de zonas.");
            $('#btnContinuarTZ').attr("disabled", true);
            setTimeout(function () { $('#btnContinuarTZ').attr("disabled", false); }, 3000);
        }
    });

    //Seleccionac todas las zonas
    $("#chkST").on("click", function () {
        $(".case").prop("checked", this.checked);
    });

    $(".case").on("click", function () {
        if ($(".case").length == $(".case:checked").length) {
            $("#chkST").prop("checked", true);
        } else {
            $("#chkST").prop("checked", false);
        }
    });

    var options = [];

    $('.dropdown-menu div').on('click', function (event) {

        var $target = $(event.currentTarget),
            val = $target.attr('value'), 
            $inp = $target.find('input'),
            idx;

        if ((idx = options.indexOf(val)) > -1) {
            options.splice(idx, 1);
            setTimeout(function () { $inp.prop('checked', false) }, 0);
        } else {
            options.push(val);
            setTimeout(function () { $inp.prop('checked', true) }, 0);
        }

        $(event.target).blur();

        return false;
    });

    $('#btnContinuarNZ').click("click", function () {
        if ($('#chkST').prop('checked')) {
            alertify.success("Todas las zonas estan seleccionadas.");
            $('#btnContinuarNZ').attr("disabled", true);
            $('#chkST').attr("disabled", true);
            $('#chkZ1').attr("disabled", true);
            $('#chkZ2').attr("disabled", true);
            $('#chkZ3').attr("disabled", true);
            $('#divTL').show();
            $('#divTMD').show();
        }else if ($('#chkZ1').prop('checked') || $('#chkZ2').prop('checked') || $('#chkZ3').prop('checked')) {
            alertify.success("Una o varias zonas seleccionadas.");
            $('#btnContinuarNZ').attr("disabled", true);
            $('#chkST').attr("disabled", true);
            $('#chkZ1').attr("disabled", true);
            $('#chkZ2').attr("disabled", true);
            $('#chkZ3').attr("disabled", true);
            $('#divTL').show();
            $('#divTMD').show();
        } else {
            alertify.error("Selecciona una o varias zonas.");
            $('#btnContinuarNZ').attr("disabled", true);
            setTimeout(function () { $('#btnContinuarNZ').attr("disabled", false); }, 3000);
        }
    });   

    $('#btnContinuarTMD').click("click", function () {
        if ($('#chkRE').prop('checked')) {
            if ($('#chkF').prop('checked') || $('#chkG').prop('checked') || $('#chkFI').prop('checked')) {
                alertify.success("Una o más evidencias seleccionadas de reporte entregado.");
                $('#btnContinuarTMD').attr("disabled", true);
                $('#chkRE').attr("disabled", true);
                $('#chkF').attr("disabled", true);
                $('#chkG').attr("disabled", true);
                $('#chkFI').attr("disabled", true);
                $('#chkRD').attr("disabled", true);
                $('#sltFE').attr("disabled", true);
                $('#divTMR').show();
            } else {
                alertify.error("Seleccione una o más evidencias");
                $('#btnContinuarTMD').attr("disabled", true);
                setTimeout(function () { $('#btnContinuarTMD').attr("disabled", false); }, 3000);
            }
            if ($('#chkF').prop('checked')) {
                if ($('#sltFE').val() != 0) {
                    alertify.success("Evidencia de la fotografia seleccionada.");
                    $('#btnContinuarTMD').attr("disabled", true);
                    setTimeout(function () { $('#btnContinuarTMD').attr("disabled", false); }, 3000);
                } else {
                    alertify.error("Falta la evidencia de la fotografia.");
                    $('#btnContinuarTMD').attr("disabled", false);
                    $('#chkRE').attr("disabled", false);
                    $('#chkF').attr("disabled", false);
                    $('#chkG').attr("disabled", false);
                    $('#chkFI').attr("disabled", false);
                    $('#chkRD').attr("disabled", false);
                    $('#sltFE').attr("disabled", false);
                    $('#btnContinuarTMD').attr("disabled", true);
                    setTimeout(function () { $('#btnContinuarTMD').attr("disabled", false); }, 3000);
                }
            }

            if ($('#chkRE').prop('checked') && $('#chkRD').prop('checked')) {
                $('#divCRD').show();
                $('#divTMR').show();
            }

        } else if ($('#chkRD').prop('checked')) {
            alertify.success("Reporte devuelto seleccionado.");
            $('#btnContinuarTMD').attr("disabled", true);
            $('#chkRE').attr("disabled", true);
            $('#chkF').attr("disabled", true);
            $('#chkG').attr("disabled", true);
            $('#chkFI').attr("disabled", true);
            $('#chkRD').attr("disabled", true);
            $('#sltFE').attr("disabled", true);
            $('#divCRD').show();
        } else {
            alertify.error("Seleccione el tipo de movimiento de distribucion.")
            $('#btnContinuarTMD').attr("disabled", true);
            setTimeout(function () { $('#btnContinuarTMD').attr("disabled", false); }, 3000);
        }
    });

    //Seleccionar todos los causales de Reporte devuelto
    $("#chkSTRD").on("click", function () {
        $(".caseRD").prop("checked", this.checked);
    });

    $(".caseRD").on("click", function () {
        if ($(".caseRD").length == $(".caseRD:checked").length) {
            $("#chkSTRD").prop("checked", true);
        } else {
            $("#chkSTRD").prop("checked", false);
        }
    });

    $('#btnContinuarCRD').on("click", function () {

        if ($("#chkSTRD").prop("checked") || $("#chkDI").prop("checked")) {

            if ($("#chkSTRD").prop("checked")) {
                if ($('#chkFRDST').prop('checked') || $('#chkFORDST').prop('checked')) {
                    alertify.success("Una o más evidencias seleccionadas de reporte devuelto.");
                    $('#btnContinuarCRD').attr("disabled", true);
                    $('#chkSTRD').attr("disabled", true);
                    $('#chkFRDST').attr("disabled", true);
                    $('#sltFRDST').attr("disabled", true);
                    $('#chkFORDST').attr("disabled", true);
                    $('#chkDI').attr("disabled", true);
                    $('#chkFDI').attr("disabled", true);
                    $('#sltFDI').attr("disabled", true);
                    $('#chkFODI').attr("disabled", true);
                    $('#divTMR').show();
                } else {
                    alertify.error("Seleccione una o más evidencias");
                    $('#btnContinuarCRD').attr("disabled", true);
                    setTimeout(function () { $('#btnContinuarCRD').attr("disabled", false); }, 3000);
                }
                if ($('#chkFRDST').prop('checked')) {
                    if ($('#sltFRDST').val() != 0) {
                        alertify.success("Evidencia de la fotografia seleccionada.");
                        $('#btnContinuarCRD').attr("disabled", true);
                        $('#chkSTRD').attr("disabled", true);
                        $('#chkFRDST').attr("disabled", true);
                        $('#sltFRDST').attr("disabled", true);
                        $('#chkFORDST').attr("disabled", true);
                        $('#chkDI').attr("disabled", true);
                        $('#chkFDI').attr("disabled", true);
                        $('#sltFDI').attr("disabled", true);
                        $('#chkFODI').attr("disabled", true);
                        $('#divTMR').show();
                    } else {
                        alertify.error("Falta la evidencia de la fotografia.");
                        $('#btnContinuarCRD').attr("disabled", true);
                        setTimeout(function () { $('#btnContinuarCRD').attr("disabled", false); }, 3000);
                        $('#chkSTRD').attr("disabled", false);
                        $('#chkFRDST').attr("disabled", false);
                        $('#sltFRDST').attr("disabled", false);
                        $('#chkFORDST').attr("disabled", false);
                        $('#chkDI').attr("disabled", false);
                        $('#chkFDI').attr("disabled", false);
                        $('#sltFDI').attr("disabled", false);
                        $('#chkFODI').attr("disabled", false);
                    }
                }
            }

            if ($("#chkDI").prop("checked")) {
                if ($('#chkFDI').prop('checked') || $('#chkFODI').prop('checked')) {
                    alertify.success("Una o más evidencias seleccionadas de reporte devuelto.");
                    $('#btnContinuarCRD').attr("disabled", true);
                    $('#chkSTRD').attr("disabled", true);
                    $('#chkFRDST').attr("disabled", true);
                    $('#sltFRDST').attr("disabled", true);
                    $('#chkFORDST').attr("disabled", true);
                    $('#chkDI').attr("disabled", true);
                    $('#chkFDI').attr("disabled", true);
                    $('#sltFDI').attr("disabled", true);
                    $('#chkFODI').attr("disabled", true);
                    $('#divTMR').show();
                } else {
                    alertify.error("Seleccione una o más evidencias");
                    $('#btnContinuarCRD').attr("disabled", true);
                    setTimeout(function () { $('#btnContinuarCRD').attr("disabled", false); }, 3000);
                }
                if ($('#chkFDI').prop('checked')) {
                    if ($('#sltFDI').val() != 0) {
                        alertify.success("Evidencia de la fotografia seleccionada.");
                        $('#btnContinuarCRD').attr("disabled", true);
                        $('#chkSTRD').attr("disabled", true);
                        $('#chkFRDST').attr("disabled", true);
                        $('#sltFRDST').attr("disabled", true);
                        $('#chkFORDST').attr("disabled", true);
                        $('#chkDI').attr("disabled", true);
                        $('#chkFDI').attr("disabled", true);
                        $('#sltFDI').attr("disabled", true);
                        $('#chkFODI').attr("disabled", true);
                    } else {
                        alertify.error("Falta la evidencia de la fotografia.");
                        $('#btnContinuarCRD').attr("disabled", true);
                        setTimeout(function () { $('#btnContinuarCRD').attr("disabled", false); }, 3000);
                        $('#chkSTRD').attr("disabled", false);
                        $('#chkFRDST').attr("disabled", false);
                        $('#sltFRDST').attr("disabled", false);
                        $('#chkFORDST').attr("disabled", false);
                        $('#chkDI').attr("disabled", false);
                        $('#chkFDI').attr("disabled", false);
                        $('#sltFDI').attr("disabled", false);
                        $('#chkFODI').attr("disabled", false);
                        $('#divTMR').show();
                    }
                }
            }

        } else {
            alertify.error("Seleccione una o más causales");
            $('#btnContinuarCRD').attr("disabled", true);
            setTimeout(function () { $('#btnContinuarCRD').attr("disabled", false); }, 3000);
        }

        

    });

    $('#btnContinuarTMR').click("click", function () {
        if ($('#chkRR').prop('checked')) {

            if ($('#chkRF').prop('checked') || $('#chkRG').prop('checked') || $('#chkRFI').prop('checked')) {
                alertify.success("Una o más evidencias seleccionadas de reporte recogido.");
                $('#btnContinuarTMR').attr("disabled", true);
                $('#chkRR').attr("disabled", true);
                $('#chkRF').attr("disabled", true);
                $('#chkRG').attr("disabled", true);
                $('#chkRFI').attr("disabled", true);
                $('#chkNR').attr("disabled", true);
            } else {
                alertify.error("Seleccione una o más evidencias");
                $('#btnContinuarTMR').attr("disabled", true);
                setTimeout(function () { $('#btnContinuarTMR').attr("disabled", false); }, 3000);
            }

            if ($('#chkRF').prop('checked')) {
                if ($('#sltRFE').val() != 0) {
                    alertify.success("Evidencia de la fotografia seleccionada.");
                    $('#btnContinuarTMR').attr("disabled", true);
                    $('#chkRR').attr("disabled", true);
                    $('#chkRF').attr("disabled", true);
                    $('#chkRG').attr("disabled", true);
                    $('#chkRFI').attr("disabled", true);
                    $('#chkNR').attr("disabled", true);
                } else {
                    alertify.error("Falta la evidencia de la fotografia.");
                    $('#btnContinuarTMR').attr("disabled", false);
                    $('#chkRR').attr("disabled", false);
                    $('#chkRF').attr("disabled", false);
                    $('#chkRG').attr("disabled", false);
                    $('#chkRFI').attr("disabled", false);
                    $('#chkNR').attr("disabled", false);
                    $('#btnContinuarTMR').attr("disabled", true);
                    setTimeout(function () { $('#btnContinuarTMR').attr("disabled", false); }, 3000);
                }
            }

            $('#save').show();

            if ($('#chkRR').prop('checked') && $('#chkNR').prop('checked')) {
                $('#divCNR').show();
                $('#save').hide();
            }

        } else if ($('#chkNR').prop('checked')) {
            alertify.success("No recogido seleccionado.");
            $('#btnContinuarTMR').attr("disabled", true);
            $('#chkRR').attr("disabled", true);
            $('#chkRF').attr("disabled", true);
            $('#chkRG').attr("disabled", true);
            $('#chkRFI').attr("disabled", true);
            $('#chkNR').attr("disabled", true);
            $('#divCNR').show();
        } else {
            alertify.error("Seleccione el tipo de movimiento de recoleccion.")
            $('#btnContinuarTMR').attr("disabled", true);
            setTimeout(function () { $('#btnContinuarTMR').attr("disabled", false); }, 3000);
        }
    });

    //Seleccionar todos los causales de No recogido
    $("#chkSTNR").on("click", function () {
        $(".caseNR").prop("checked", this.checked);
    });

    $(".caseNR").on("click", function () {
        if ($(".caseNR").length == $(".caseNR:checked").length) {
            $("#chkSTNR").prop("checked", true);
        } else {
            $("#chkSTNR").prop("checked", false);
        }
    });

    $('#btnContinuarCNR').click("click", function () {

        if ($("#chkSTNR").prop("checked") || $("#chkCSG").prop("checked")) {

            if ($("#chkSTNR").prop("checked")) {
                if ($('#chkFNRST').prop('checked') || $('#chkFONRST').prop('checked')) {
                    alertify.success("Una o más evidencias seleccionadas de no recogido.");
                    $('#btnContinuarCNR').attr("disabled", true);
                    $('#chkSTNR').attr("disabled", true);
                    $('#chkFNRST').attr("disabled", true);
                    $('#sltFNRST').attr("disabled", true);
                    $('#chkFONRST').attr("disabled", true);
                    $('#chkCSG').attr("disabled", true);
                    $('#chkFCG').attr("disabled", true);
                    $('#sltFCG').attr("disabled", true);
                    $('#chkFOCG').attr("disabled", true);
                } else {
                    alertify.error("Seleccione una o más evidencias");
                    $('#btnContinuarCNR').attr("disabled", true);
                    setTimeout(function () { $('#btnContinuarCNR').attr("disabled", false); }, 3000);
                }
                if ($('#chkFNRST').prop('checked')) {
                    if ($('#sltFNRST').val() != 0) {
                        alertify.success("Evidencia de la fotografia seleccionada.");
                        $('#btnContinuarCNR').attr("disabled", true);
                        $('#chkSTNR').attr("disabled", true);
                        $('#chkFNRST').attr("disabled", true);
                        $('#sltFNRST').attr("disabled", true);
                        $('#chkFONRST').attr("disabled", true);
                        $('#chkCSG').attr("disabled", true);
                        $('#chkFCG').attr("disabled", true);
                        $('#sltFCG').attr("disabled", true);
                        $('#chkFOCG').attr("disabled", true);
                        $('#save').show();
                    } else {
                        alertify.error("Falta la evidencia de la fotografia.");
                        $('#btnContinuarCNR').attr("disabled", true);
                        setTimeout(function () { $('#btnContinuarCNR').attr("disabled", false); }, 3000);
                        $('#chkSTNR').attr("disabled", false);
                        $('#chkFNRST').attr("disabled", false);
                        $('#sltFNRST').attr("disabled", false);
                        $('#chkFONRST').attr("disabled", false);
                        $('#chkCSG').attr("disabled", false);
                        $('#chkFCG').attr("disabled", false);
                        $('#sltFCG').attr("disabled", false);
                        $('#chkFOCG').attr("disabled", false);
                    }
                }
            }

            if ($("#chkCSG").prop("checked")) {
                if ($('#chkCSG').prop('checked') || $('#chkFODI').prop('checked')) {
                    alertify.success("Una o más evidencias seleccionadas de no recogido.");
                    $('#btnContinuarCNR').attr("disabled", true);
                    $('#chkSTNR').attr("disabled", true);
                    $('#chkFNRST').attr("disabled", true);
                    $('#sltFNRST').attr("disabled", true);
                    $('#chkFONRST').attr("disabled", true);
                    $('#chkCSG').attr("disabled", true);
                    $('#chkFCG').attr("disabled", true);
                    $('#sltFCG').attr("disabled", true);
                    $('#chkFOCG').attr("disabled", true);                    
                } else {
                    alertify.error("Seleccione una o más evidencias");
                    $('#btnContinuarCNR').attr("disabled", true);
                    setTimeout(function () { $('#btnContinuarCNR').attr("disabled", false); }, 3000);
                }
                if ($('#chkFCG').prop('checked')) {
                    if ($('#sltFCG').val() != 0) {
                        alertify.success("Evidencia de la fotografia seleccionada.");
                        $('#btnContinuarCNR').attr("disabled", true);
                        $('#chkSTNR').attr("disabled", true);
                        $('#chkFNRST').attr("disabled", true);
                        $('#sltFNRST').attr("disabled", true);
                        $('#chkFONRST').attr("disabled", true);
                        $('#chkCSG').attr("disabled", true);
                        $('#chkFCG').attr("disabled", true);
                        $('#sltFCG').attr("disabled", true);
                        $('#chkFOCG').attr("disabled", true);
                    } else {
                        alertify.error("Falta la evidencia de la fotografia.");
                        $('#btnContinuarCNR').attr("disabled", true);
                        setTimeout(function () { $('#btnContinuarCNR').attr("disabled", false); }, 3000);
                        $('#chkSTNR').attr("disabled", false);
                        $('#chkFNRST').attr("disabled", false);
                        $('#sltFNRST').attr("disabled", false);
                        $('#chkFONRST').attr("disabled", false);
                        $('#chkCSG').attr("disabled", false);
                        $('#chkFCG').attr("disabled", false);
                        $('#sltFCG').attr("disabled", false);
                        $('#chkFOCG').attr("disabled", false);                        
                    }
                }
                $('#save').show();
            }

        } else {
            alertify.error("Seleccione una o más causales");
            $('#btnContinuarCNR').attr("disabled", true);
            setTimeout(function () { $('#btnContinuarCNR').attr("disabled", false); }, 3000);
        }

        

    });

    $('#save').click("click", function () {
        alertify.success("Evidencio guardada correctamente.");
        $('#save').attr("disabled", true);
    });

});